#include <windows.h>
#include <stdio.h>
#include <string>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE /*hPrevInstance*/,
        LPSTR /*szCmdLine*/, int iCmdShow)
{
    static char szAppName[] = "hatchbrushbug";
    HWND        hwnd;
    MSG         msg;
    WNDCLASSEX  wndclass;


    wndclass.cbSize         = sizeof(wndclass);
    wndclass.style          = CS_HREDRAW | CS_VREDRAW;
    wndclass.lpfnWndProc    = WndProc;
    wndclass.cbClsExtra     = 0;
    wndclass.cbWndExtra     = 0;
    wndclass.hInstance      = hInstance;
    wndclass.hIcon          = NULL;
    wndclass.hIconSm        = NULL;
    wndclass.hCursor        = LoadCursor(NULL, IDC_ARROW);
    wndclass.hbrBackground  = (HBRUSH) GetStockObject(WHITE_BRUSH);
    wndclass.lpszClassName  = szAppName;
    wndclass.lpszMenuName   = NULL;


    RegisterClassEx(&wndclass);

    hwnd = CreateWindow(szAppName, "Wine Hatch Brush Bug",
            WS_OVERLAPPEDWINDOW,
            CW_USEDEFAULT, CW_USEDEFAULT,
            300, 300,
            NULL, NULL, hInstance, NULL);


    ShowWindow(hwnd, iCmdShow);
    UpdateWindow(hwnd);

    while ( GetMessage(&msg, NULL, 0, 0) )
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

bool direct=true;
bool transparent=true;

void drawPattern(HDC dc, bool direct, std::string text, int w, int h)
{
    HDC target=dc;
    if(!direct)
        dc=CreateCompatibleDC(target);
    HBITMAP bmp=NULL;
    int saved=SaveDC(dc);
    if(!direct)
    {
        bmp=CreateCompatibleBitmap(target, w, h);
        SelectObject(dc, bmp);
    }
    SelectObject(dc, GetStockObject(GRAY_BRUSH));
    Rectangle(dc, 0, 0, w, h);
    HBRUSH br=CreateHatchBrush(HS_BDIAGONAL, RGB(255, 0, 127));
    SelectObject(dc, br);
    SetBkMode(dc, transparent?TRANSPARENT:OPAQUE);
    Rectangle(dc, w*1/10, h*1/10, w*9/10, h*9/10);
    ExtTextOut(dc, 0, 0, 0, 0, text.c_str(), text.size(), NULL);
    if(!direct)
        BitBlt(target, 0, 0, w, h, dc, 0, 0, SRCCOPY);
    RestoreDC(dc, saved);
    DeleteObject(br);
    if(bmp)
    {
        DeleteObject(bmp);
        DeleteDC(dc);
    }
}

std::string getSettingsStr()
{
    std::string str;
    str+=direct?"direct ":"indirect ";
    str+=transparent?"transparent ":"opaque ";
    return str;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
    switch ( iMsg )
    {
        case WM_ERASEBKGND:
            return 0;

        case WM_LBUTTONUP:
            direct=!direct;
            InvalidateRect(hwnd, NULL, TRUE);
            return 0;

        case WM_RBUTTONUP:
            transparent=!transparent;
            InvalidateRect(hwnd, NULL, TRUE);
            return 0;

        case WM_MBUTTONUP:
            {
                HDC hdc=GetDC(NULL);
                drawPattern(hdc, direct, getSettingsStr(), 300, 300);
                ReleaseDC(NULL, hdc);
            }
            return 0;

        case WM_PAINT:
            {
                RECT r;
                GetClientRect(hwnd, &r);
                int w=r.right-r.left, h=r.bottom-r.top;
                PAINTSTRUCT ps;
                HDC hdc = BeginPaint(hwnd, &ps);
                drawPattern(hdc, direct, getSettingsStr(), w, h);
                EndPaint(hwnd, &ps);
            }
            return 0;

        case WM_DESTROY:
            PostQuitMessage(0);
            return 0;
    }
    return DefWindowProc(hwnd, iMsg, wParam, lParam);
}
