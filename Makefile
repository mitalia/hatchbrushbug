TARGETWINE=hatchbrushbug-wine.exe
TARGETWIN=hatchbrushbug-win.exe
CXX=/opt/mingw32-dw2/bin/i686-w64-mingw32-g++
CC=/opt/mingw32-dw2/bin/i686-w64-mingw32-gcc
WINECXX=wineg++
WINECC=winegcc
LIBS=-lgdi32

all: win wine

win: ${TARGETWIN}

wine: ${TARGETWINE}

${TARGETWIN}: hatchbrushbug.cpp
	${CXX} -static hatchbrushbug.cpp ${LIBS} -o ${TARGETWIN}

${TARGETWINE}: hatchbrushbug.cpp
	${WINECXX} hatchbrushbug.cpp ${LIBS} -o ${TARGETWINE}

clean:
	rm ${TARGETWIN} ${TARGETWINE} ${TARGETWINE}.so
